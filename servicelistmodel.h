// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2020 Aleix Quintana Alsius <kinta@communia.org>

#ifndef SERVICELISTMODEL_H
#define SERVICELISTMODEL_H

#include <QAbstractListModel>
#include <QLoggingCategory>

/**
 * @todo write docs
 */
class ServiceItem
{
public:
    ServiceItem() {}
    ServiceItem(
        const QString& id, 
        const QString& displayName, 
        const QString& hostName, 
        const QString& lastCheckOutput,
        const int& state,
        const QString& zone,
        const QMap<QString, QVariant> raw
    )
    : id(id)
    , displayName(displayName)
    , hostName(hostName)
    , lastCheckOutput(lastCheckOutput)
    , state(state)
    , zone(zone)
    , raw(raw)
    {}

    QString id;
    QString displayName;
    QString hostName;
    QString lastCheckOutput;
    int state;
    QString zone;
    QMap<QString, QVariant> raw;
};

//Q_DECLARE_METATYPE(ServiceItem)
//Q_DECLARE_TYPEINFO(ServiceItem, Q_MOVABLE_TYPE);

class ServiceListModel : public QAbstractListModel {
    Q_PROPERTY(int count READ count NOTIFY countChanged)
    Q_OBJECT
public:
    // from printf "0x%08X\n" $(($RANDOM*$RANDOM))
    enum Roles {
        DisplayNameRole = 0x166BD2F0,
        ServiceIdRole = 0x058A505D,
        HostNameRole = 0x051A78F2,
        LastCheckOutputRole = 0x013D0A99,
        RawRole = 0x0758D495,
        ServiceStateRole = 0x263D1E90,
        ZoneRole = 0x100EB6B6
    };
    explicit ServiceListModel(QObject *parent = nullptr);
    ServiceListModel(const ServiceItem& item, QObject *parent = nullptr);
    virtual ~ServiceListModel();
    int count() const;
    // QAbstractItemModel interface
public:
    int rowCount(const QModelIndex&) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole) override;
    QHash<int, QByteArray> roleNames() const override;
public slots:
    void insert(const ServiceItem& item, int i);
    void insert(const QVector<ServiceItem>& items, int index);
    void append(const QList<ServiceItem>& items);
    void append(const ServiceItem& item);
    /**
     * If list is empty or the item in index i is not there: append
     * else : replace the row.
     * 
     * @param int i
     *   the index
     * @param ServiceItem item
     *   the item data
     */
    void appendOrReplace(int i, const ServiceItem& item);
    /**
     * Trims the model starting from the index until the end of the list. 
     * 
     * @param index
     *   the index which will be the start of the trim 
     */
    void trim(int index);
    //Q_INVOKABLE bool remove(const ServiceItem& item);
    void removeAt(int index);
    void clear();
private:
    QList<ServiceItem> m_services;
signals:
    void countChanged();
};
Q_DECLARE_LOGGING_CATEGORY(ICINGAENGINE);

#endif // SERVICEDATAOBJECT_H
